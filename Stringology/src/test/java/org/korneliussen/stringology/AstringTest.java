package org.korneliussen.stringology;

import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.Test;

public class AstringTest {

	@Test
	public void testBigintBinary() {
		Alphabet b= new Alphabet("01");
		
		bIHelp(b, "", 0l);
		bIHelp(b, "0", 1l);
		bIHelp(b, "1", 2l);
		bIHelp(b, "00", 3l);
		bIHelp(b, "01", 4l);
		bIHelp(b, "10", 5l);
		bIHelp(b, "11", 6l);
		bIHelp(b, "000", 7l);
	}
	
	private void bIHelp(Alphabet a, String s, long l) {
		assertTrue(new Astring(a, s).toBigint().equals(BigInteger.valueOf(l)));
	}
	
	@Test
	public void testBigintTrinary() {
		Alphabet t = new Alphabet("abc");
		bIHelp(t,"",0l);
		bIHelp(t,"a",1l);
		bIHelp(t,"b",2l);
		bIHelp(t,"c",3l);
		bIHelp(t,"aa",4l);
		bIHelp(t,"ab",5l);
		bIHelp(t,"ac",6l);
		bIHelp(t,"ba",7l);
		bIHelp(t,"bb",8l);
		bIHelp(t,"bc",9l);
		bIHelp(t,"ca",10l);
		bIHelp(t,"cb",11l);
		bIHelp(t,"cc",12l);
		bIHelp(t,"aaa",13l);
	}
	
	@Test
	public void testBigintUnary() {
		Alphabet u = new Alphabet("a");
		bIHelp(u,"",0l);
		bIHelp(u,"a",1l);
		bIHelp(u,"aa",2l);
	}
	
	private void bIConsHelper(Alphabet a, String s, long l) {
		assertEquals(new Astring(a, s), new Astring(a, BigInteger.valueOf(l)));
	}
	
	@Test
	public void testBigintConstructorBinary() {
		Alphabet b = new Alphabet("01");
		bIConsHelper(b, "", 0l);
		bIConsHelper(b, "0", 1l);
		bIConsHelper(b, "1", 2l);
		bIConsHelper(b, "00", 3l);
		bIConsHelper(b, "01", 4l);
		bIConsHelper(b, "10", 5l);
		bIConsHelper(b, "11", 6l);
		bIConsHelper(b, "000", 7l);
		bIConsHelper(b, "001", 8l);
		bIConsHelper(b, "010", 9l);
		//assertEquals(new Astring(b, ""), new Astring(b, BigInteger.ZERO));
	}
	
	private void bijectiveHelper(Alphabet a, String s) {
		Astring as = new Astring(a,s);
		assertEquals(as, new Astring(a,as.toBigint()));
	}
	
	@Test
	public void bigintBijective() {
		Alphabet b = new Alphabet("01");
		bijectiveHelper(b, "0010110");
		bijectiveHelper(b, "00101101101110");
		Alphabet u = new Alphabet("a");
		bijectiveHelper(u, "aaaaaaaaaaaaaaaaaaaaaaa");
		bijectiveHelper(u, "aaaaaaaa");
		Alphabet t = new Alphabet("abc");
		bijectiveHelper(t, "aaaaaaaa");
		bijectiveHelper(t, "abacaaba");
		bijectiveHelper(t, "abacaabcaabc");
		Alphabet o = new Alphabet("01234567");
		bijectiveHelper(o, "0034755516272415354625354525151511511003475551627241535462535452515151151100347555162724153546253545251515115110034755516272415354625354525151511511");
	}
}
