package org.korneliussen.stringology;

import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.Test;

public class AMatrixTest {

	@Test
	public void test() {
		Alphabet b = new Alphabet("012");
		
		int[][] mat1 = {{0}};
		
		AMatrix am = new AMatrix(b,mat1);
		System.out.println("[0] is " + am.toBigInteger());
		am = new AMatrix(b,am.toBigInteger());
		System.out.println(am);
		
		mat1 = new int[][]{{0,0},{0,0}};
		am = new AMatrix(b,mat1);
		am = new AMatrix(b,am.toBigInteger());
		System.out.println(am);
		
		mat1 = new int[][]{{0,0,0}
						  ,{2,1,0}
						  ,{0,0,0}};
		am = new AMatrix(b,mat1);
		am = new AMatrix(b,am.toBigInteger());
		System.out.println(am);
		
		mat1 = new int[][]{{1},{1}};
		am = new AMatrix(b,mat1);
		am = new AMatrix(b,am.toBigInteger());
		System.out.println(am);
		
		mat1 = new int[][]{{0,1,0}
						  ,{0,0,0}
						  ,{0,0,0}};
		am = new AMatrix(b,mat1);
		am = new AMatrix(b,am.toBigInteger());
		System.out.println(am);
	}

	@Test
	public void prints() {
		/*Alphabet b = new Alphabet("01");
		
		System.out.println(new AMatrix(b,BigInteger.ZERO));
		System.out.println(new AMatrix(b,BigInteger.ONE));
		System.out.println(new AMatrix(b,BigInteger.valueOf(2)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(3)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(4)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(5)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(6)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(7)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(8)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(9)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(10)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(11)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(12)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(13)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(14)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(15)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(16)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(17)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(18)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(19)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(20)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(21)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(22)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(23)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(24)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(25)));
		System.out.println(new AMatrix(b,BigInteger.valueOf(26)));*/
	}
}
