package org.korneliussen.stringology;

import java.math.BigInteger;
import java.util.ArrayList;

public class Astring {
	Alphabet alphabet;
	int[] data;
	
	public Astring(Alphabet a, String in) {
		alphabet = a;
		data = new int[in.length()];
		
		for (int i = 0; i < in.length(); i++) {
			data[i] = a.toIndex(in.charAt(i));
		}
	}
	
	public Astring(Alphabet a, BigInteger in) {

		alphabet = a;
		
		BigInteger remainder = in;
		BigInteger power = BigInteger.ONE;
		BigInteger asize = BigInteger.valueOf(a.size());
		BigInteger prev = BigInteger.ZERO;
		int size = 0;
		while (remainder.signum() > 0) {
			size++;
			prev = remainder;
			power = power.multiply(asize);
			remainder = remainder.subtract(power);
		}
		data = new int[size];
		prev = prev.subtract(BigInteger.ONE); // It has to be this way to be correct, but why?
		
		for (int i = 0; i < data.length; i++) {
			data[data.length - i - 1] = prev.mod(asize).intValue();
			prev = prev.divide(asize);									
		}
	}
	
	public BigInteger toBigint() {
		BigInteger ret = BigInteger.ZERO;
		BigInteger asize = BigInteger.valueOf(alphabet.size());
		
		if (alphabet.size == 0)
			return ret;
		
		for (int i = 0; i < data.length; i++) {
			ret = ret.add(asize.pow(i));
			ret = ret.add(asize.pow(data.length - i - 1).multiply(BigInteger.valueOf(data[i])));
		}
		return ret;
	}
	
	
	
	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		
		for (int c : data){
			ret.append(alphabet.toRep(c));
			ret.append(",");
		}
		if (ret.length() > 0)
			ret.deleteCharAt(ret.length() - 1);
		return ret.toString();
	}
	
	@Override
	public boolean equals(Object o) {
		try {
			Astring other = (Astring) o;
			if (!this.alphabet.equals(other.alphabet))
				return false;
			if (this.data.length != other.data.length)
				return false;
			for (int i = 0; i < data.length; i++) {
				if (this.data[i] != other.data[i])
					return false;
			}
			return true;
		} catch (ClassCastException c) {
			return false;
		}
	}
	
	// Assumes no bugs in toBigInt.
	@Override
	public int hashCode() {
		return toBigint().hashCode();
	}
}
