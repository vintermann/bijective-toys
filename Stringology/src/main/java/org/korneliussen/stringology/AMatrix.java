package org.korneliussen.stringology;

import java.math.BigInteger;

public class AMatrix {

	Alphabet alphabet;
	BigInteger aSize;
	int[][] matrix;
	int rows, cols;
	
	public AMatrix(Alphabet a, int[][] m) {
		alphabet = a;
		aSize = BigInteger.valueOf(a.size());
		matrix = m; // No validity checking
		
		rows = matrix.length;
		cols = matrix[0].length; // They'd better all be that length.
		
		// This bijection is from positive integers to matrices containing elements.
		
		
		/*
		{
			{} {} {} {}
			{} {} {} {}
		}*/
	}
	
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder("[");
		for (int row = 0; row < rows; row++) {
			for (int col = 0; col < cols; col++){
				b.append(matrix[row][col]);
				b.append(",");
			}
			b.deleteCharAt(b.length() - 1);
			b.append(";\n ");
		}
		b.delete(b.length() - 3, b.length());
		b.append("]");
		return b.toString();
	}
	
	public AMatrix(Alphabet a, BigInteger in) {
		alphabet = a;
		aSize = BigInteger.valueOf(a.size());
		BigInteger candidate = aSize.subtract(BigInteger.ONE);
		BigInteger lastStep = null;
		int row = 1; 
		int col = 1;
		while (in.subtract(candidate).signum() > 0) {

			if (col == 1) { // go on to next cube
				col = row + 1;
				row = col;
			} else if (row == 1) { // reached bottom, go back to top and turn left
				row = col;
				col--;
			} else if (col >= row) { // go down
				row--;
			} else { // going left
				col--;
			}
			lastStep = aSize.pow(row * col);
			candidate = candidate.add(lastStep);
		}

		matrix = new int[row][col];
		rows = row;
		cols = col;
		
		if (lastStep == null)
			candidate = in;
		else 
			candidate = in.subtract(candidate.subtract(lastStep)).subtract(BigInteger.ONE);
		System.out.println("Candidate: " +candidate );
		BigInteger[] divRem = null;
		for (row = 0; row < rows; row++) {
			for (col = 0; col < cols; col++){
				divRem = candidate.divideAndRemainder(aSize);
				matrix[row][col] = divRem[1].intValue();
				candidate = divRem[0]; 
			}
		}
		System.out.println("Remainder: " + divRem[1] + " " + divRem[0]);
	}
	
	public BigInteger toBigInteger() {
		int row = 1;
		int col = 1;

		BigInteger current = BigInteger.ZERO;
		
		// Find the right dims
		while (row != rows || col != cols) {
			current = current.add(aSize.pow(row * col));
			
			if (col == 1) { // go on to next cube
				col = row + 1;
				row = col;
			} else if (row == 1) { // reached bottom, go back to top and turn left
				row = col;
				col--;
			} else if (col >= row) { // go down
				row--;
			} else { // going left
				col--;
			}
		}
		for (row = 0; row < rows; row++) {
			for (col = 0; col < cols; col++){
				current = current.add( 
						aSize.pow((col + row * cols))
							 .multiply(BigInteger.valueOf(matrix[row][col]))); 
			}
		}
		return current;
	}
}
