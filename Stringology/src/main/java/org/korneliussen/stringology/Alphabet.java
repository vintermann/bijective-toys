package org.korneliussen.stringology;

public class Alphabet {
	int size;
	String representation;
	
	public Alphabet(String in) {
		representation = in;
		size = in.length();
	}
	
	public Alphabet(int size) {
		this.size = size;
		representation = null; 
	}
	
	public int toIndex(char c) {
		return representation.indexOf(c);
	}
	
	public String toRep(int i) {
		if (representation != null)
			return "" + representation.charAt(i);
		else
			return "" + i;
	}
	
	public int size() {
		return size;
	}
}
