package org.korneliussen.stringology;

import java.math.BigInteger;

public class Main {
	public static void main(String[] args) {
		Alphabet o = new Alphabet("01");
		
		int[][] mat1 = {{1,0,0,1},
					   {0,1,1,1},
					   {0,0,1,1}};
		
		int [][] mat2 = {
				{0,0,0},
				{0,0,0},
				{0,0,1}
		};
		AMatrix am = new AMatrix(o,mat1);
		AMatrix back = new AMatrix(o,am.toBigInteger());
		System.out.println(back);
	}
}